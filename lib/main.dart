import 'package:flutter/material.dart';
import 'page/provider.dart';
import '_testmain.dart';
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.\

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "App Test",
      home: Provider(),
    );
  }
}


