import 'package:flutter/material.dart';
import 'package:testwidgets/_testmain.dart';
import 'package:testwidgets/page/provider_body.dart';
import 'package:testwidgets/widgets/mycolors.dart';
import 'package:testwidgets/widgets/provider_head.dart';

class Provider extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Column(
        children: <Widget>[
          ProviderHead(),
          ProviderBody()
        ],
      ),
    );
  }
}