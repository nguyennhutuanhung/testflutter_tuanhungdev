import 'package:flutter/material.dart';

/*Use simple color in my project*/
class MyColors{
  /*List color*/
//  UseColor._();

  /* Provider */
  static const Color blue_3E3F68 = Color(0xFF3E3F68);
  static const Color brown_A84E4E = Color(0xFFA84E4E);
  static const Color pink_FF6381 = Color(0xFFFF6381);
  static const Color orange_FF4A40 = Color(0xFFFF4A40);

}