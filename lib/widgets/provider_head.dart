import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:testwidgets/widgets/mycolors.dart';

class ProviderHead extends StatelessWidget{
  /*Search:
    1. BackGround
    2. ButtonTop
    3. ButtonBottom
   */
  @override
  Widget build(BuildContext context) {
    double height = 2435;
    double width = 1125;
    double maxheight = MediaQuery.of(context).size.height;
    double maxwidth = MediaQuery.of(context).size.width;
    double alphaheight = maxheight/height;
    double alphawidth = maxwidth/width;

    print("$alphaheight, $alphawidth");
    // TODO: implement build
    return Stack(
        children: <Widget>[
          //BackGround
          Container(
            height: 909*alphaheight,
            width: width*alphawidth,
            child: Image.asset(
              "assets/images/bgheadfix.png",
              fit: BoxFit.fill,
            ),
          ),
          //Gradient
          Container(
            height: 503*alphaheight,
            width: width*alphawidth,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [Colors.black.withOpacity(0.6), Colors.transparent]
                )
            ),
          ),
          //ButtonTop
          Positioned(
            top: 102*alphaheight,
            left: 40*alphawidth,
            child: Container(
              height: 108*alphaheight,
              width: 73*alphawidth,
              child: RawMaterialButton(
                child: Image.asset(
                  "assets/images/iconleft.png",
                  fit: BoxFit.fill,
                ),
                onPressed: (){},
              )
            ),
          ),
          Positioned(
            top: 95*alphaheight,
            right: 34*alphawidth,
            child: Row(
              children: <Widget>[
                Container(
                  child: Container(
                    height: 122*alphaheight,
                    width: 104*alphawidth,
                    child: RawMaterialButton(
                      child: Image.asset(
                        "assets/images/iconright1.png",
                        fit: BoxFit.cover,
                      ),
                      onPressed: (){},
                    ),
                  ),

                ),
                SizedBox(width: 38*alphawidth,),
                Container(
                  height: 122*alphaheight,
                  width: 103*alphawidth,
                  child: RawMaterialButton(
                    child: Image.asset(
                      "assets/images/iconright.png",
                      fit: BoxFit.cover,
                    ),
                    onPressed: (){},
                  ),
                ),
              ],
            ),
          ),
          //ButtonBottom
          Positioned(
            bottom: 50*alphaheight,
            left: 91*alphawidth,
            right: 91*alphawidth,
            child: Container(
              constraints: BoxConstraints(
                maxHeight: 142.5*alphaheight,
                maxWidth: 943*alphaheight,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(50),
                      bottomLeft: Radius.circular(50)
                    ),
                    child: BackdropFilter(
                      filter: ImageFilter.blur(
                        sigmaX: 5,
                        sigmaY: 5
                      ),
                      child: RaisedButton(
                        color: Colors.transparent,
                        padding: EdgeInsets.only(
                            left: 63*alphawidth,
                            right: 63*alphawidth,
                            top: 34*alphaheight,
                            bottom: 34*alphaheight),
                        child: Container(
                          height: 142.5*alphaheight,
                          child: Row(
                            children: <Widget>[
                              Container(
                                child: Image.asset(
                                  "assets/images/iconphone.png",
                                  fit: BoxFit.fitHeight,),
                              ),
                              SizedBox(width: 34*alphawidth,),
                              Text(
                                "+1 212-673-3754",
                                style: TextStyle(
                                    fontSize: 37*alphaheight,
                                    color: Colors.white
                                ),
                              ),
                            ],
                          ),
                        ),
                        onPressed: (){},
                      ),
                    ),
                  ),
                  VerticalDivider(
                    width: 0.1,
                    color: Colors.white.withOpacity(0.5),),
                  ClipRRect(
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(50),
                        bottomRight: Radius.circular(50)
                    ),
                    child: BackdropFilter(
                      filter: ImageFilter.blur(
                        sigmaX: 5,
                        sigmaY: 5
                      ),
                      child: RaisedButton(
                        color: Colors.transparent,
                        padding: EdgeInsets.only(
                            left: 63*alphawidth,
                            right: 63*alphawidth,
                            top: 34*alphaheight,
                            bottom: 34*alphaheight),
                        child: Container(
                          height: 142.5*alphaheight,
                          child: Row(
                            children: <Widget>[
                              Container(
                                child: Image.asset(
                                  "assets/images/icondirectory.png",
                                  fit: BoxFit.fitHeight,),
                              ),
                              SizedBox(width: 34*alphaheight,),
                              Text(
                                "Direction",
                                style: TextStyle(
                                    fontSize: 37*alphaheight,
                                    color: Colors.white
                                ),
                              ),
                            ],
                          ),
                        ),
                        onPressed: (){},
                      ),
                    ),
                  )
                ],
              ),
            )
          ),
        ]
    );
  }
}